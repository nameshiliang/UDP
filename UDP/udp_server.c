/***************************************************
> Copyright (C) 2020 ==shiliang== All rights reserved.
> File Name: udp_server.c
> Author:shiliang
> Mail:675052779@qq.com 
> Created Time: 2020年06月04日 星期四 16时53分42秒
***************************************************/

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <unistd.h>

#define PORT 10000
#define IP	127.0.0.1

/*
实现使用UDP进行socket通信。客户端给服务器发信息；服务器接收。
在同一台PC上进行测试时候，IP地址可以写成127.0.0.1，如果是两
个人用不同的PC进行测试，地址一定要写对方的IP地址
*/
int main(int argc,const char* argv[])
{
	int sockfd = 0;
	struct sockaddr_in seraddr,cliaddr;
	int flag = 0;
	socklen_t addrlen = 0;
	addrlen = sizeof(struct sockaddr_in);
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	if(-1 == sockfd)
	{
		perror("socket");
		exit(1);
	}
	else
	{
		puts("create socket success\n");
	}
	//绑定自己的端口和地址
	memset(&seraddr,'\0',sizeof(struct sockaddr_in));
	seraddr.sin_family = AF_INET;
	seraddr.sin_port = htons(atoi(argv[2]));
	seraddr.sin_addr.s_addr = inet_addr(argv[1]);
	flag = bind(sockfd,(struct sockaddr*)&seraddr,sizeof(seraddr));
	if(-1 == flag)
	{
		perror("bind");
		exit(1);
	}
	else
	{
		puts("socket bind success\n");
		ssize_t rv = 0;
		char buf[100] = {0};
		rv = recvfrom(sockfd,buf,sizeof(buf),0,(struct sockaddr*)&cliaddr,&addrlen);
		if(0>rv)
		{
			perror("recvfrom");
			exit(1);
		}
		else
		{
			printf("recv:%s-%ld\n",buf,rv);
		}
		close(sockfd);
	}
    return 0;
}
