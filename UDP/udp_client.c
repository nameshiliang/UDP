/***************************************************
> Copyright (C) 2020 ==shiliang== All rights reserved.
> File Name: udp_client.c
> Author:shiliang
> Mail:675052779@qq.com 
> Created Time: 2020年06月05日 星期五 19时00分56秒
***************************************************/

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <unistd.h>

/*实现使用UDP进行通信。客户端给服务器发信息，服务器接收。
在同一台PC上进行测试时候，IP地址可以写成127.0.0.1
如果是两个人用不同的PC进行测试，地址一定要写对方的IP地址*/

int main(int argc,const char* argv[])
{
	int sockfd = 0;
	char buf[100] = {0};
	struct sockaddr_in  seraddr;
	sockfd = socket(AF_INET,SOCK_DGRAM,0);
	if(0>sockfd)
	{
		perror("sockfd");
		exit(1);
	}
	else
	{
		printf("socket create success\n");

	}
	//设置对方的地址和端口
	memset(&seraddr,'\0',sizeof(struct sockaddr_in));
	seraddr.sin_family = AF_INET;
	seraddr.sin_port = htons(atoi(argv[2]));
	seraddr.sin_addr.s_addr = inet_addr(argv[1]);
	//发送消息
	
	int ret = 0;
	memset(buf,'\0',sizeof(buf));
	puts("input the msg you want to send\n");
	scanf("%s",buf);
	ret = sendto(sockfd,buf,sizeof(buf),0,(struct sockaddr*)&seraddr,sizeof(seraddr));
	if(0>ret)
	{
		perror("sendto");
		exit(1);
	}
	else
	{
		puts("message send success\n");
	}
	close(sockfd);
    return 0;
}
